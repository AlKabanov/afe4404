/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 ******************************************************************************
 * @section License
 * <b>Copyright 2015 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "gpio.h"
#include "rtc.h"
#include "uart.h"
#include "i2c.h"

#define MARG_MAX 20000
#define MARG_MIN -20000
#define INT32TO16(x)  (int16_t)((x)>>6)
void setADCFlag(void);		

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/

volatile bool ADCFlag = false;
uint32_t resultCounter = 0,waitAFE = 0,AFECounter = 0;
uint8_t uartData[29] = "START";
int16_t led1,led2,led3,ambi;
int led1Offset = 0, led2Offset = 0, led3Offset = 0;
bool offset = false;
uint32_t led1Curr = 40, led2Curr = 40, led3Curr = 40;
bool current = false;

void clockLoop(void)
{
  gpioSetCallBack(setADCFlag);
  //i2cAFEOffset(led1Offset,led2Offset,led3Offset);
  //i2cAFECurrent(led1Curr,led2Curr,led3Curr);
  i2cAFEStart();
  //i2cAFEOffset(-5, -1, -1);
  
  while (1)
  {
    //EMU_EnterEM2(true);
    if(ADCFlag)waitAFE++;               //check if result is ready
    while (ADCFlag == false);
    resultCounter ++;
    ADCFlag = false;
    gpioToggleLED();
    
    led1 = INT32TO16(i2cGetLED1());
    led2 = INT32TO16(i2cGetLED2());
    led3 = INT32TO16(i2cGetLED3());
    //ambi = INT32TO16(i2cGetAMBI());
    uartData[5] = led1 >> 8;
    uartData[6] = led1;   //X1
    uartData[7] = led2 >> 8;
    uartData[8] = led2;         //Y1
    uartData[9] = led3 >> 8;
    uartData[10] = led3;         //Z1
    uartData[11] = 0;
    uartData[12] = rtcGetDifmSeconds();        //X2
    if((led1 > MARG_MAX)&&(led1Offset > -15)){led1Offset--;offset = true;}
    if((led1 < MARG_MIN)&&(led1Offset < 15)){led1Offset++;offset = true;}
    if((led2 > MARG_MAX)&&(led2Offset > -15)){led2Offset--;offset = true;}
    if((led2 < MARG_MIN)&&(led2Offset < 15)){led2Offset++;offset = true;}
    if((led3 > MARG_MAX)&&(led3Offset > -15)){led3Offset--;offset = true;}
    if((led3 < MARG_MIN)&&(led3Offset < 15)){led3Offset++;offset = true;}
    if((led1 > 32000)&&(led1Offset == -15)&&(led1Curr > 0)){led1Curr--;current = true;}
    if((led1 < -32000)&&(led1Offset == 15)&&(led1Curr < 50)){led1Curr++;current = true;}
    if((led2 > 32000)&&(led2Offset == -15)&&(led2Curr > 0)){led2Curr--;current = true;}
    if((led2 < -32000)&&(led2Offset == 15)&&(led2Curr < 50)){led2Curr++;current = true;}
    if((led3 > 32000)&&(led3Offset == -15)&&(led3Curr > 0)){led3Curr--;current = true;}
    if((led3 < -32000)&&(led3Offset == 15)&&(led3Curr < 50)){led3Curr++;current = true;}
    
    if(offset)
    {
      i2cAFEOffset(led1Offset,led2Offset,led3Offset);
      offset = false;
    }
    if(current)
    {
      i2cAFECurrent(led1Curr,led2Curr,led3Curr);
      current = false;
    }
    
    uartSendArr(uartData, 29/*sizeof(uartData)*/);
 
 /*   
    uartSend(0x12);
   
    uartSend(led2);
    uartSend(led2>>8);
    uartSend(led2>>16);
    uartSend(led2>>24);
    
    uartSend(led3);
    uartSend(led3>>8);
    uartSend(led3>>16);
    uartSend(led3>>24);
    
    uartSend(led1);
    uartSend(led1>>8);
    uartSend(led1>>16);
    uartSend(led1>>24);
    
    uartSend(0x13);
  */  
    
    //rtcWait(20);    
  }
}

void setADCFlag(void)
{ 
  i2cReadADC();
  i2cSumm();
  if(++AFECounter == 4)
  {
    ADCFlag = true;
    AFECounter = 0;
    i2cAver(4);
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();


  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();

  /* Setup GPIO interrupt to set the time */
  gpioSetup();
  
  uartInit(9600);
  i2cAFEInit(led1Curr, led2Curr, led3Curr);
  
  gpioSetLED();
  __no_operation();
  gpioClearLED();  
  
  /* Main function loop */
  clockLoop();

  return 0;
}
/*
int16_t int32toint16(int32_t data)
{
  int32_t temp = data>>7;
  if(data) 
}
*/