#include <em_cmu.h>
#include <em_emu.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include "gpio.h"
#include "uart.h"

#define LEUART_TX_PORT                 gpioPortD
#define LEUART_TX_BIT                  4

/** Enable flag for UART */
static LEUART_Enable_TypeDef PortEnable = leuartDisable;

void uartInit(uint32_t baudRate)
{
  
  //GPIO_PinModeSet(gpioPortD, 0, gpioModePushPull, 1);
  
  LEUART_Init_TypeDef init = LEUART_INIT_DEFAULT;                                         // defining the LEUART0 initialization data
  
  if (baudRate > 9600)
  {
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_4);
    init.refFreq = 1750000;                                                             // 14MHz/2 prescaled by 4
  }
  else
  {
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_1);
    init.refFreq = 0;
  }

  
  CMU_ClockEnable(cmuClock_LEUART0, true);                                                // enabling the LEUART0 clock
  
  init.enable     = leuartDisable;
  init.baudrate   = baudRate;
  
  LEUART_Reset(LEUART0);                                                                  // reseting and initializing LEUART0
  LEUART_Init(LEUART0, &init);
  
  /* Configure GPIO pins */
  GPIO_PinModeSet(LEUART_TX_PORT, LEUART_TX_BIT, gpioModePushPull, 1);                                  // TX PORT
  PortEnable = leuartEnableTx;                                                            // always enable tx
 
  LEUART0->ROUTE = LEUART_ROUTE_TXPEN | LEUART_ROUTE_LOCATION_LOC0;
  LEUART0->CMD = LEUART_CMD_TXDIS | LEUART_CMD_CLEARTX;
  LEUART0->CMD = LEUART_CMD_TXEN;
  
  LEUART_Enable(LEUART0, PortEnable);

}
void uartSend(uint8_t byte)
{
  LEUART_Tx(LEUART0, byte);
}

void uartSendArr(uint8_t *data, uint8_t len)
{
  for(int i = 0; i< len;i++)LEUART_Tx(LEUART0, data[i]);
}
