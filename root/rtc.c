#include "em_emu.h"
#include "em_cmu.h"
#include "em_rtc.h"
#include "rtc.h"

#define RTC_FREQ    32768

volatile uint32_t mSeconds = 0;


/**************************************************************************//**
 * @brief RTC Interrupt Handler.
 *        Updates minutes and hours.
 *****************************************************************************/
void RTC_IRQHandler(void)
{
  /* Clear interrupt source */
  RTC_IntClear(RTC_IFC_COMP0);

  /* Increase time by one minute */
  //minutes++;
  mSeconds++;
}


/**************************************************************************//**
 * @brief Enables LFACLK and selects LFXO as clock source for RTC
 *        Sets up the RTC to generate an interrupt every minute.
 *****************************************************************************/
void rtcSetup(void)
{
  RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

  /* Enable LE domain registers */
  CMU_ClockEnable(cmuClock_CORELE, true);

  /* Enable LFXO as LFACLK in CMU. This will also start LFXO */
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

  /* Set a clock divisor of 32 to reduce power consumption. */
  //CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);

  /* Enable RTC clock */
  CMU_ClockEnable(cmuClock_RTC, true);

  /* Initialize RTC */
  rtcInit.enable   = false;  /* Do not start RTC after initialization is complete. */
  rtcInit.debugRun = false;  /* Halt RTC when debugging. */
  rtcInit.comp0Top = true;   /* Wrap around on COMP0 match. */
  RTC_Init(&rtcInit);

  /* Interrupt every minute */
 //RTC_CompareSet(0, ((RTC_FREQ / 32) * 60 ) - 1 );
  /* Interrupt every 10 sec */
  //RTC_CompareSet(0, ((RTC_FREQ / 32) * 10 ) - 1 );
  /* Interrupt every 1 msec */
  RTC_CompareSet(0, (RTC_FREQ / 1024) - 1 );

  /* Enable interrupt */
  NVIC_EnableIRQ(RTC_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0);

  /* Start Counter */
  RTC_Enable(true);
}

void rtcWait(uint32_t mSec)
{
 uint32_t startTime = mSeconds;
 uint32_t endTime = startTime + mSec;
 while(mSeconds < endTime);
 return;
}

uint32_t prevmSec = 0;
uint32_t rtcGetDifmSeconds(void)
{
  uint32_t dif = mSeconds - prevmSec;
  prevmSec = mSeconds;
  return dif;
}

