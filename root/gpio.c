
#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include "gpio.h"

#define LED_PORT      gpioPortC           
#define LED_BIT       15				
#define LED_MASK      (1 << LED_BIT)

#define PIN_SPI_CS                13			//CSN = PE13
#define PORT_SPI_CS               gpioPortE

#define RST_PORT gpioPortB
#define RST_PIN  14

#define AFERST_PORT gpioPortC
#define AFERST_PIN 14

#define DRDY_PORT gpioPortC
#define DRDY_PIN 0

#define LED_EN  GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModePushPull, 0)
#define LED_DIS GPIO_PinModeSet(LED_PORT, LED_BIT, gpioModeDisabled, 0)
#define LED_ON  GPIO_PinOutSet(LED_PORT, LED_BIT)
#define LED_OFF GPIO_PinOutClear(LED_PORT, LED_BIT)
#define LED_TOGGLE GPIO_PinOutToggle(LED_PORT, LED_BIT)

#define LDO_PORT      gpioPortF           
#define LDO_BIT       2		
#define LDO_EN  GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModePushPull, 0)
#define LDO_DIS GPIO_PinModeSet(LDO_PORT, LDO_BIT, gpioModeDisabled, 0)
#define LDO_ON  GPIO_PinOutSet(LDO_PORT, LDO_BIT)
#define LDO_OFF GPIO_PinOutClear(LDO_PORT, LDO_BIT)

void defaultCallBack(void);

typedef void (*callback_t)(void);
callback_t evenCallBack = defaultCallBack;

void gpioSetCallBack(void (*callback)(void))
{
  evenCallBack = callback;
}

void defaultCallBack(void)
{
}

/**************************************************************************//**
 * @brief Setup GPIO interrupt to set the time
 *****************************************************************************/
void gpioSetup(void)
{
  /* Enable GPIO clock */
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  GPIO_PinModeSet(PORT_SPI_CS, PIN_SPI_CS, gpioModePushPull, 1);
  
  GPIO_PinModeSet(AFERST_PORT, AFERST_PIN, gpioModePushPull, 1);

  //Configure DRDY as input  (PC0)
  GPIO_PinModeSet(DRDY_PORT, DRDY_PIN, gpioModeInput, 1);

  // Set falling edge interrupt 
  GPIO_IntConfig(DRDY_PORT, DRDY_PIN, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  
/*
  // Configure PB9 as input 
  GPIO_PinModeSet(gpioPortB, 9, gpioModeInput, 0);

  // Set falling edge interrupt 
  GPIO_IntConfig(gpioPortB, 9, false, true, true);
  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
*/
  LED_EN;
  LDO_EN;
}

void gpioSetLED(void)
{
  LED_ON;
}

void gpioClearLED(void)
{
  LED_OFF;
}

void gpioToggleLED(void)
{
  LED_TOGGLE;
}
void gpioLDOOn(void)
{
  LDO_ON;
}

void gpioLDOOff(void)
{
  LDO_OFF;
}

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val)
{
  if(val == 0 || val == 1)
  { // drive pin
       GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModePushPull, 0);
       if (val)GPIO_PinOutSet(RST_PORT, RST_PIN);
       else    GPIO_PinOutClear(RST_PORT, RST_PIN);
  } else 
    { // keep pin floating
        GPIO_PinModeSet(RST_PORT, RST_PIN, gpioModeDisabled, 1);
    }
}

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val)
{
	//not used 
}

// set radio NSS pin to given value
void gpioNSS (uint8_t val)
{
  if (val) GPIO_PinOutSet(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Disable
		
  else GPIO_PinOutClear(PORT_SPI_CS, PIN_SPI_CS);	//  SPI Enable (Active Low)
		
}

void gpioAFERST( uint8_t val)
{
  if (val) GPIO_PinOutSet(AFERST_PORT, AFERST_PIN);	//  SPI Disable
		
  else GPIO_PinOutClear(AFERST_PORT, AFERST_PIN);	//  SPI Enable (Active Low)
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB9)
 *        Sets the hours
 *****************************************************************************/
/*
void GPIO_ODD_IRQHandler(void)
{
  // Acknowledge interrupt 
  GPIO_IntClear(1 << 9);

  // Increase minutes 
  hours = (hours + 1) % 24;
}
*/
/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB10)
 *        Sets the minutes
 *****************************************************************************/

void GPIO_EVEN_IRQHandler(void)
{
  // Acknowledge interrupt 
  GPIO_IntClear(1 << DRDY_PIN);
  evenCallBack();
  
}
