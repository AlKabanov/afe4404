/***************************************************************************//**
 * @file
 * @brief I2C AFE4404 pulse meter
 * @author Kabanov
 * @version 1.0.0
  ******************************************************************************/

#include <em_i2c.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include "gpio.h"
#include "i2c.h"

#define SDA_PORT gpioPortA
#define SDA_BIT  0
#define SCL_PORT gpioPortA
#define SCL_BIT  1

#define I2Cx I2C0

#define AFE_ADDR (0x58 << 1)

#define CONTROL                 0
#define REPEAT_COUNT            0x1D
#define LED_2_START             9
#define LED_2_END               0x0A
#define SLED_2_START            1
#define SLED_2_END              2
#define ADC_RST_0_START         0x15
#define ADC_RST_0_END           0x16
#define CONV_LED_2_START        0xD
#define CONV_LED_2_END          0xE
#define LED_3_START             0x36
#define LED_3_END               0x37
#define SLED_3_START            5
#define SLED_3_END              6
#define ADC_RST_1_START         0x17
#define ADC_RST_1_END           0x18
#define CONV_LED_3_START        0x0F
#define CONV_LED_3_END          0x10
#define LED_1_START             3
#define LED_1_END               4
#define SLED_1_START            7
#define SLED_1_END              8
#define ADC_RST_2_START         0x19
#define ADC_RST_2_END           0x1A
#define CONV_LED_1_START        0x11
#define CONV_LED_1_END          0x12
#define SAMBI_START             0x0B
#define SAMBI_END               0x0C
#define ADC_RST_3_START         0x1B
#define ADC_RST_3_END           0x1C
#define CONV_AMBI_START         0x13
#define CONV_AMBI_END           0x14
#define PWR_DOWN_START          0x32
#define PWR_DOWN_END            0x33

#define PWR_DOWN_BIT    0
#define OSC_EN_BIT      9
#define I_LED3_BITS     12
#define I_LED2_BITS     6
#define I_LED1_BITS     0
#define TIMER_EN_BIT    8
    
#define LED1_OFFSET_POL         9
#define LED2_OFFSET_POL         19
#define LED3_OFFSET_POL         4
#define AMB_OFFSET_POL          14
#define LED1_OFFSET_BITS        5 
#define LED2_OFFSET_BITS        15
#define LED3_OFFSET_BITS        0 
#define AMB_OFFSET_BITS         10 

//enable read registers
#define READ_REG_MODE     write3Reg(CONTROL,1)  
#define WRITE_REG_MODE    write3Reg(CONTROL,0) 
#define SW_RESET          write3Reg(CONTROL,8) 







#define PDN_START 0x32
#define PDN_END 0x33


const I2C_Init_TypeDef g_i2cInit = I2C_INIT_DEFAULT;

uint32_t readVal = 0;

typedef struct
{
  int32_t LED1;
  int32_t LED2;
  int32_t LED3;
  int32_t ambient;
  int32_t sumLED1;
  int32_t sumLED2;
  int32_t sumLED3;
  int32_t averLED1;
  int32_t averLED2;
  int32_t averLED3;
} ADC_t;
typedef enum
{
  gain_500k = 0,
  gain_250k = 1,
  gain_100k = 2,
  gain_50k  = 3,
  gain_25k = 4,
  gain_10k = 5,
  gain_1M = 6,
  gain_2M = 7,
}AFEGain_t;


ADC_t  ADCVal;

static int write3Reg(uint8_t regAddr, uint32_t data);
static uint32_t read3Reg(uint8_t regAddr);
static void AFEpowerDown(bool val);
static void oscEnable(bool val);
static void timerEnable(bool val);
static void LEDCurrent(uint32_t LED1, uint32_t LED2, uint32_t LED3);
static void DACOffset(int LED1,int LED2, int LED3);
static void setGain(AFEGain_t gain);
//static void setDecimation(bool val);


void i2cAFEInit(uint32_t LED1, uint32_t LED2, uint32_t LED3)
{
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_I2C0, true);
    
    GPIO_PinModeSet(SDA_PORT, SDA_BIT, gpioModeWiredAnd, 1);
    GPIO_PinModeSet(SCL_PORT, SCL_BIT, gpioModeWiredAnd, 1);
    
    for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
    
    /* Enable pins at location 0*/
    I2Cx->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN | (0 << _I2C_ROUTE_LOCATION_SHIFT);
                  
    I2C_Init(I2Cx, &g_i2cInit);
    
    SW_RESET; //reset
    
    AFEpowerDown(true);
      
    write3Reg(REPEAT_COUNT,39999);
    
    write3Reg(LED_2_START,0); // LED_2_START time
    write3Reg(LED_2_END,399);
     
    write3Reg(SLED_2_START,80); 
    write3Reg(SLED_2_END,399); 
    
    write3Reg(ADC_RST_0_START,401); 
    write3Reg(ADC_RST_0_END,407); 
    
    write3Reg(CONV_LED_2_START,408); 
    write3Reg(CONV_LED_2_END,1467); 
    
    write3Reg(LED_3_START,400); // LED_3_START time
    write3Reg(LED_3_END,799);
     
    write3Reg(SLED_3_START,480); 
    write3Reg(SLED_3_END,799); 
    
    write3Reg(ADC_RST_1_START,1469); 
    write3Reg(ADC_RST_1_END,1475); 
    
    write3Reg(CONV_LED_3_START,1476); 
    write3Reg(CONV_LED_3_END,2535); 
    
    write3Reg(LED_1_START,800); // LED_1_START time
    write3Reg(LED_1_END,1199);
     
    write3Reg(SLED_1_START,880); 
    write3Reg(SLED_1_END,1199); 
    
    write3Reg(ADC_RST_2_START,2537); 
    write3Reg(ADC_RST_2_END,2543); 
    
    write3Reg(CONV_LED_1_START,2544); 
    write3Reg(CONV_LED_1_END,3603); 
    
    write3Reg(SAMBI_START,1279); // Ambient light measuring time
    write3Reg(SAMBI_END,1598);
     
    write3Reg(ADC_RST_3_START,3605); 
    write3Reg(ADC_RST_3_END,3611); 
    
    write3Reg(CONV_AMBI_START,3612); 
    write3Reg(CONV_AMBI_END,4671); 
       
    write3Reg(PWR_DOWN_START,5471); 
    write3Reg(PWR_DOWN_END,39199);
    
    setGain(gain_500k);
    
    LEDCurrent(LED1, LED2, LED3);
    DACOffset(0,0,0);
    //setDecimation(true);
    oscEnable(true);
    timerEnable(true);
   
    
    READ_REG_MODE;
       
    readVal = read3Reg(LED_2_END);
    readVal = read3Reg(LED_3_END);  
    readVal = read3Reg(LED_1_END);
    
    
    readVal = read3Reg(0x23);    
    readVal = read3Reg(0x1e); 
    readVal = read3Reg(0x22);
        
}

void i2cAFEStart(void)
{
  AFEpowerDown(false);
}

void i2cAFEOffset(int LED1, int LED2, int LED3)
{
  AFEpowerDown(true);
  DACOffset(LED1,LED2,LED3);
  AFEpowerDown(false);
}
void i2cAFECurrent(uint32_t LED1, uint32_t LED2, uint32_t LED3)
{
  AFEpowerDown(true);
  LEDCurrent(LED1, LED2, LED3);
  AFEpowerDown(false);
}
void i2cReadADC(void)
{
  //READ_REG_MODE;  
  uint32_t temp1 = read3Reg(0x2c);
  uint32_t temp2 = read3Reg(0x2a);
  uint32_t temp3 = read3Reg(0x2b);
  //uint32_t temp4 = read3Reg(0x2d); 
  
  ADCVal.LED1 = (temp1 & (0x400000))?temp1|0xFFC00000:temp1;

  ADCVal.LED2 = (temp2 & (0x400000))?temp2|0xFFC00000:temp2;
 
  ADCVal.LED3 = (temp3 & (0x400000))?temp3|0xFFC00000:temp3;
  
  //ADCVal.ambient = (temp4 & (0x400000))?temp4|0xFFC00000:temp4;
}
void i2cSumm(void)
{
  ADCVal.sumLED1 +=  ADCVal.LED1;
  ADCVal.sumLED2 +=  ADCVal.LED2;
  ADCVal.sumLED3 +=  ADCVal.LED3;
}

void i2cAver(int denominator)
{
  ADCVal.averLED1 = ADCVal.sumLED1/denominator;
  ADCVal.averLED2 = ADCVal.sumLED2/denominator;
  ADCVal.averLED3 = ADCVal.sumLED3/denominator;
  ADCVal.sumLED1 = 0;
  ADCVal.sumLED2 = 0;
  ADCVal.sumLED3 = 0;
}

int32_t i2cGetLED1(void)
{
  //return ADCVal.LED1;
  return ADCVal.averLED1;
}
int32_t i2cGetLED2(void)
{
  //return ADCVal.LED2;
  return ADCVal.averLED2;
}
int32_t i2cGetLED3(void)
{
  //return ADCVal.LED3;
  return ADCVal.averLED3;
}
int32_t i2cGetAMBI(void)
{
  return ADCVal.ambient;
}


static void setGain(AFEGain_t gain)
{
   uint32_t temp = read3Reg(0x21);
   temp &= 0xFFF8U; 
   temp |= gain;
   WRITE_REG_MODE;
   write3Reg(0x21,temp);
}

static void DACOffset(int LED1,int LED2, int LED3)
{
  uint32_t temp = 0;
  if(LED1 > 15)LED1 = 15;
  if(LED1 < -15)LED1 = -15;
  if(LED2 > 15)LED2 = 15;
  if(LED2 < -15)LED2 = -15;
  if(LED3 > 15)LED3 = 15;
  if(LED3 < -15)LED3 = -15;
  
  if(LED1 < 0) 
  {
    temp |= 1U << LED1_OFFSET_POL;
    LED1 = LED1 * (-1); 
  }
  if(LED2 < 0)
  {
    temp |= 1U << LED2_OFFSET_POL;
    LED2 = LED2 * (-1); 
  }
  if(LED3 < 0)
  {
    temp |= 1U << LED3_OFFSET_POL;
    LED3 = LED3 * (-1); 
  }
  
  temp |= (LED1 << LED1_OFFSET_BITS);
  temp |= (LED2 << LED2_OFFSET_BITS);
  temp |= (LED3 << LED3_OFFSET_BITS);
  //temp |= (1U << AMB_OFFSET_POL)|(1U << AMB_OFFSET_BITS);
  WRITE_REG_MODE;
  write3Reg(0x3a,temp);
}
/*
static void setDecimation(bool val)
{
  uint32_t temp = 0;
  temp = val?0x14:0; 
  WRITE_REG_MODE;
  write3Reg(0x3d,temp);
}
*/
//write 3 bytes register after initialization
static int write3Reg(uint8_t regAddr, uint32_t data)
{
  uint8_t dataToSend[4];
  
  I2C_TransferSeq_TypeDef i2cFrame;
	
  i2cFrame.addr = AFE_ADDR;
  int retCode;
  
  dataToSend[0] = regAddr;
  dataToSend[1] = (uint8_t)(data >> 16);
  dataToSend[2] = (uint8_t)(data >> 8);
  dataToSend[3] = (uint8_t)data;
  
  i2cFrame.flags = I2C_FLAG_WRITE;
  i2cFrame.buf[0].data = dataToSend;
  i2cFrame.buf[0].len = 4;
  
  //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
  //use polled transfer
  while ((retCode = I2C_Transfer(I2Cx)) == i2cTransferInProgress);
  
  return(retCode);
  
}

//read 3 byte register after initialization



static uint32_t read3Reg(uint8_t regAddr)
{
  I2C_TransferSeq_TypeDef i2cFrame;
  uint8_t regToRead[3];
  uint32_t regRead;
	
  i2cFrame.addr = AFE_ADDR;
	
  //int retCode;
  
  i2cFrame.flags = I2C_FLAG_WRITE_READ;
     
	
  i2cFrame.buf[0].data = &regAddr;
  i2cFrame.buf[0].len = 1;

          
  i2cFrame.buf[1].data = regToRead;
  i2cFrame.buf[1].len = 3;
          
          //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
          
          //use polled transfer
  while (I2C_Transfer(I2Cx) == i2cTransferInProgress);
  
  regRead = (((uint32_t)regToRead[0])<<16)+(((uint32_t)regToRead[1])<<8)+regToRead[2];
  //if(regRead & 0x800000 )reg
    
  return(regRead);
   
}

static void AFEpowerDown(bool val)
{
  uint32_t temp;
  READ_REG_MODE;
  temp = read3Reg(0x23);  
  if(val == true)   temp |= 1<<PWR_DOWN_BIT;            //power down mode
  else temp &= ~(1<<PWR_DOWN_BIT);                      //normal mode
  WRITE_REG_MODE;
  write3Reg(0x23,temp);
}

static void oscEnable(bool val)
{
  uint32_t temp;
  READ_REG_MODE;
  temp = read3Reg(0x23);  
  if(val == true)   temp |= 1U<<OSC_EN_BIT;            //oscillator enable
  else temp &= ~(1U<<OSC_EN_BIT);                      //oscillator disable
  WRITE_REG_MODE;
  write3Reg(0x23,temp);
}

static void LEDCurrent(uint32_t LED1, uint32_t LED2, uint32_t LED3)
{
  if(LED1 > 63)LED1 = 63;
  if(LED2 > 63)LED2 = 63;
  if(LED3 > 63)LED3 = 63;
  WRITE_REG_MODE;
  write3Reg(0x22,(LED1 << I_LED1_BITS)|(LED2 << I_LED2_BITS)|(LED3 << I_LED3_BITS));
}

static void timerEnable(bool val)
{
  uint32_t temp;
  READ_REG_MODE;
  temp = read3Reg(0x1e);  
  if(val == true)   temp |= 1U<<TIMER_EN_BIT;            //oscillator enable
  else temp &= ~(1U<<TIMER_EN_BIT);                      //oscillator disable
  WRITE_REG_MODE;
  write3Reg(0x1e,temp);
}
