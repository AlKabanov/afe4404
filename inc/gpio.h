



void gpioSetup(void);
void gpioSetLED(void);
void gpioClearLED(void);
void gpioToggleLED(void);

void gpioLDOOn(void);
void gpioLDOOff(void);

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val);

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val);

// set radio NSS pin to given value
void gpioNSS (uint8_t val);

//reset AFE (50uS to 0)
void gpioAFERST( uint8_t val);

// function called in even interrupt
void gpioSetCallBack(void (*callback)(void));