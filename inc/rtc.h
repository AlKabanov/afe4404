
/**************************************************************************//**
 * @brief Enables LFACLK and selects LFXO as clock source for RTC
 *        Sets up the RTC to generate an interrupt every minute.
 *****************************************************************************/
void rtcSetup(void);

void rtcWait(uint32_t mSec);

uint32_t rtcGetDifmSeconds(void);