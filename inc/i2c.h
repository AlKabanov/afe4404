
void i2cAFEInit(uint32_t LED1, uint32_t LED2, uint32_t LED3);

void i2cAFEStart(void);

//read LED1 LED2 LED3 ambient
void i2cReadADC(void);

int32_t i2cGetLED1(void);  //green

int32_t i2cGetLED2(void);  //red

int32_t i2cGetLED3(void);  //infrared

int32_t i2cGetAMBI(void);

void i2cAFEOffset(int LED1, int LED2, int LED3);

void i2cAFECurrent(uint32_t LED1, uint32_t LED2, uint32_t LED3);

void i2cSumm(void);

void i2cAver(int denominator);